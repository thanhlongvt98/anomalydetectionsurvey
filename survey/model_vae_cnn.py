import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np 
import sympy

import matplotlib.pyplot as plt

class VAE(nn.Module):
    """
    VAE model.
    Use Omni-Scaled kernel size
    """
    def __init__(
        self,
        N_scales,
        hidden_num,
        input_channel,
        input_length
    ):
        """
        Init VAE model with Omni-scaled kernel size

        Args:
            N_scales: should be length_of_data/4
        """
        super().__init__()
        self.N_scales = N_scales
        self.hidden_num = hidden_num
        self.input_channel = input_channel
        self.input_length = input_length

        self.prime_list = [p for p in range(self.N_scales) if sympy.isprime(p)]
        self.p_L = len(self.prime_list)
        print('Kernel size: ', self.prime_list)

        self.encoder_cnn_1 = nn.ModuleList([
            nn.Conv1d(
                self.input_channel, 
                self.input_channel,
                self.prime_list[i],
                padding= 1 if self.prime_list[i]==2 else int((self.prime_list[i] - 1)/2),
                stride=1,
                dilation= 2 if self.prime_list[i]==2 else 1
            ) for i in range(len(self.prime_list))
        ])
        self.encoder_batchnorm_1 = nn.BatchNorm1d(
            self.input_channel*self.p_L
        )
        self.encoder_relu_1 = nn.ReLU()
        self.encoder_cnn_2 = nn.ModuleList([
            nn.Conv1d(
                self.input_channel*self.p_L,
                self.input_channel*self.p_L,
                self.prime_list[i],
                padding=1 if self.prime_list[i]==2 else int((self.prime_list[i] - 1)/2),
                dilation=2 if self.prime_list[i]==2 else 1
            ) for i in range(len(self.prime_list))
        ])
        self.encoder_batchnorm_2 = nn.BatchNorm1d(
            self.input_channel*self.p_L*self.p_L
        )
        self.encoder_relu_2 = nn.ReLU()

        self.encoder_cnn_3 = nn.ModuleList([
            nn.Conv1d(
                self.input_channel*self.p_L*self.p_L,
                self.input_channel*self.p_L*self.p_L,
                1,
                padding=0
            ),
            nn.Conv1d(
                self.input_channel*self.p_L*self.p_L,
                self.input_channel*self.p_L*self.p_L,
                2,
                padding=1,
                dilation=2
            )           
        ])
        self.encoder_batchnorm_3 = nn.BatchNorm1d(
            2*self.input_channel*self.p_L*self.p_L
            )
        self.encoder_relu_3 = nn.ReLU()
        self.encoder_pooling = nn.AvgPool1d(
            kernel_size = self.input_length,
            padding = 0,
            stride = 1
        )
        self.encoder_fc = nn.Linear(
            2*self.input_channel*self.p_L*self.p_L,
            2*self.hidden_num
        )

        self.decoder_fc = nn.Linear(
            self.hidden_num,
            2*self.input_channel*self.p_L*self.p_L*self.input_length
        )
        self.decoder_cnn_3 = nn.ModuleList([
            nn.Conv1d(
                self.input_channel*self.p_L*self.p_L,
                self.input_channel*self.p_L*self.p_L,
                1,
                padding=0            
            ),
            nn.Conv1d(
                self.input_channel*self.p_L*self.p_L,
                self.input_channel*self.p_L*self.p_L,
                2,
                padding=1,
                dilation=2            
            )
        ])
        self.decoder_batchnorm_3 = nn.BatchNorm1d(
            self.input_channel*self.p_L*self.p_L
        )
        self.decoder_relu_3 = nn.ReLU()
        self.decoder_cnn_2 = nn.ModuleList([
            nn.Conv1d(
                self.input_channel*self.p_L,
                self.input_channel*self.p_L,
                self.prime_list[i],
                padding=1 if self.prime_list[i]==2 else int((self.prime_list[i] - 1)/2),
                dilation=2 if self.prime_list[i]==2 else 1
            ) for i in range(len(self.prime_list))
        ])
        self.decoder_batchnorm_2 = nn.BatchNorm1d(
            self.input_channel*self.p_L
        )
        self.decoder_relu_2 = nn.ReLU()
        self.decoder_cnn_1 = nn.ModuleList([
            nn.Conv1d(
                self.input_channel, 
                self.input_channel,
                self.prime_list[i],
                padding=1 if self.prime_list[i]==2 else int((self.prime_list[i] - 1)/2),
                dilation=2 if self.prime_list[i]==2 else 1            
            ) for i in range(len(self.prime_list))
        ])
        self.decoder_batchnorm_1 = nn.BatchNorm1d(self.input_channel)
        self.decoder_relu_1 = nn.ReLU()

        # TODO: Init weight 

    def encode(self, x):
        output_cnn_1 = [self.encoder_cnn_1[i](x) for i in range(self.p_L)]
        output_cnn_1_cat = torch.cat(output_cnn_1, dim=1)
        output_batchnorm_1 = self.encoder_batchnorm_1(output_cnn_1_cat)
        output_relu_1 = self.encoder_relu_1(output_batchnorm_1)

        output_cnn_2 = [self.encoder_cnn_2[i](output_relu_1) for i in range(self.p_L)]
        output_cnn_2_cat = torch.cat(output_cnn_2, dim=1)
        output_batchnorm_2 = self.encoder_batchnorm_2(output_cnn_2_cat)
        output_relu_2 = self.encoder_relu_2(output_batchnorm_2)

        output_cnn_3 = [self.encoder_cnn_3[i](output_relu_2) for i in range(2)]
        output_cnn_3_cat = torch.cat(output_cnn_3, dim=1)
        output_batchnorm_3 = self.encoder_batchnorm_3(output_cnn_3_cat)
        output_relu_3 = self.encoder_relu_3(output_batchnorm_3)

        output_pool = self.encoder_pooling(output_relu_3)
        output_pool = output_pool.reshape((output_pool.shape[0], -1))
        output_fc = self.encoder_fc(output_pool)
        return output_fc

    def decode(self, h):
        output_fc = self.decoder_fc(h)
        output_fc = output_fc.reshape(
            (output_fc.shape[0], 2*self.input_channel*self.p_L*self.p_L, self.input_length)
        )
        output_fc = torch.split(output_fc, self.input_channel*self.p_L*self.p_L, dim=1)
        output_cnn_3 = [
            self.decoder_cnn_3[i](output_fc[i]) for i in range(2)
        ]
        output_cnn_3_mean = torch.mean(torch.stack(output_cnn_3), dim=0)
        output_batchnorm_3  = self.decoder_batchnorm_3(output_cnn_3_mean)
        output_relu_3 = self.decoder_relu_3(output_batchnorm_3)

        output_relu_3 = torch.split(output_relu_3, self.input_channel*self.p_L, dim=1)

        output_cnn_2 = [self.decoder_cnn_2[i](output_relu_3[i]) for i in range(self.p_L)]
        output_cnn_2_mean = torch.mean(torch.stack(output_cnn_2), dim=0)
        output_batchnorm_2  = self.decoder_batchnorm_2(output_cnn_2_mean)
        output_relu_2 = self.decoder_relu_2(output_batchnorm_2)

        output_relu_2 = torch.split(output_relu_2, self.input_channel, dim=1)

        output_cnn_1 = [self.decoder_cnn_1[i](output_relu_2[i]) for i in range(self.p_L)]
        output_cnn_1_mean = torch.mean(torch.stack(output_cnn_1), dim=0)
        output_batchnorm_1  = self.decoder_batchnorm_1(output_cnn_1_mean)
        output_relu_1 = self.decoder_relu_1(output_batchnorm_1)

        return output_relu_1
    
    def reparameter(self, muy, sigma):
        epsilon = torch.randn_like(muy)
        z = muy + epsilon*torch.exp(sigma/2)
        return z

    def forward(self, x):
        h = self.encode(x)
        muy = h[:, 0:int(h.shape[1]/2)]
        logvar = h[:, int(h.shape[1]/2):]
        z = self.reparameter(
            muy,
            logvar 
        )
        out = self.decode(z)
        return out, muy, logvar, z
# H = 10
# N_scales = 20
# input_channel = 3
# input_length = 40

# N = 5

# vae = VAE(hidden_num=H, N_scales=N_scales, input_channel=input_channel, input_length=input_length)
# x = torch.tensor(np.arange(int(N*input_channel*input_length)), dtype=torch.float32).reshape((N, input_channel, input_length))
# # print(x.shape)
# # y = vae(x)
# # print(y)



# optimizer = optim.Adam(vae.parameters(), lr=10)
# step = 500
# vae.train()
# loss_hist = []
# for i in range(step):
#     x_hat = vae(x)
#     loss = torch.mean((x - x_hat)**2)
#     loss.backward()
#     optimizer.step()
#     optimizer.zero_grad()
#     loss_hist.append(loss.item())
#     print(loss.item())

# y = vae(x)

# print(x - y)

# plt.figure()
# plt.plot(np.arange(step), loss_hist)
# plt.show()