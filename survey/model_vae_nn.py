import torch
import torch.nn as nn
from torch.nn.modules.container import ModuleList
import torch.optim as optim
import numpy as np 
import sympy

import matplotlib.pyplot as plt

class VAE(nn.Module):
    """
    VAE model.
    Use Omni-Scaled kernel size
    """
    def __init__(
        self,
        hidden_num,
        input_channel,
        input_length,
        hidden_layer_num
    ):
        """
        Init VAE model with fully connected layer
        """
        super().__init__()
        self.hidden_num = hidden_num
        self.input_channel = input_channel
        self.input_length = input_length
        self.hidden_layer_num = hidden_layer_num

        self.encode_nn = nn.ModuleList(
            [
                nn.Linear(
                    in_features=input_channel*input_length,
                    out_features=input_channel*input_length
                )
                for i in range(hidden_layer_num)
            ]
        )
        self.encode_last = nn.Linear(
            in_features=input_channel*input_length,
            out_features=2*hidden_num
        )
        self.decode_last = nn.Linear(
            in_features=hidden_num,
            out_features=input_channel*input_length
        )
        self.decode_nn = nn.ModuleList(
            [
                nn.Linear(
                    in_features=input_channel*input_length,
                    out_features=input_channel*input_length
                )
                for i in range(hidden_layer_num)
            ]
        )

    def encode(self, x):
        out = x.reshape(-1, self.input_channel*self.input_length)
        # print(out.shape)
        for i in range(self.hidden_layer_num):
            out = self.encode_nn[i](out)
            out = nn.LeakyReLU()(out)
        out = self.encode_last(out)
        return out

    def decode(self, h):
        out = self.decode_last(h)
        for i in range(self.hidden_layer_num-1):
            out = self.decode_nn[i](out)
            out = nn.LeakyReLU()(out)
        out = self.decode_nn[self.hidden_layer_num-1](out)
        out = out.reshape(-1, self.input_channel, self.input_length)
        return out

    def reparameter(self, muy, sigma):
        epsilon = torch.randn_like(muy)
        z = muy + epsilon*torch.exp(sigma/2)
        return z

    def forward(self, x):
        h = self.encode(x)
        muy = h[:, 0:int(h.shape[1]/2)]
        logvar = h[:, int(h.shape[1]/2):]
        z = self.reparameter(
            muy,
            logvar 
        )
        out = self.decode(z)
        return out, muy, logvar, z
# H = 10
# N_scales = 20
# input_channel = 3
# input_length = 40

# N = 5

# vae = VAE(hidden_num=H, input_channel=input_channel, input_length=input_length, hidden_layer_num=10)
# x = torch.tensor(np.arange(int(N*input_channel*input_length)), dtype=torch.float32).reshape((N, input_channel, input_length))
# print(x.shape)
# y = vae(x)
# print(y[0].shape)





# optimizer = optim.Adam(vae.parameters(), lr=10)
# step = 500
# vae.train()
# loss_hist = []
# for i in range(step):
#     x_hat = vae(x)
#     loss = torch.mean((x - x_hat)**2)
#     loss.backward()
#     optimizer.step()
#     optimizer.zero_grad()
#     loss_hist.append(loss.item())
#     print(loss.item())

# y = vae(x)

# print(x - y)

# plt.figure()
# plt.plot(np.arange(step), loss_hist)
# plt.show()