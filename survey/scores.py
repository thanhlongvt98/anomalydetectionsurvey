import torch 
import numpy as np 

def log_likelihood_multivariate_normal(mean, var, x):
    var_numerical = var + 1e-8
    d = x.size()[-1]
    MD = torch.sum(((mean - x)**2)*(1.0/var_numerical))
    log_likelihood = -0.5*(d*np.log(2*np.pi) + torch.sum(torch.log(var_numerical)) + MD)
    return log_likelihood

def reconstruction_score(muy_x_hat, sigma_x_hat, x, device):
    L, N, _ = sigma_x_hat.size()
    scores = torch.zeros((L, N, 1), device=device)
    for n in range(N):
        for l in range(L):
            scores[l, n, 0] = log_likelihood_multivariate_normal(
                muy_x_hat[l, n, :], 
                sigma_x_hat[l, n, :]**2,
                x[l, n, :]
            )
    return scores