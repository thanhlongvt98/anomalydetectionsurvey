import torch
from torch._C import device
import torch.nn as nn
from torch.nn import parameter
from torch.nn.modules.container import ModuleList
import torch.optim as optim
import numpy as np 
import sympy

import matplotlib.pyplot as plt

class VAE(nn.Module):
    """
    VAE model.
    Use Omni-Scaled kernel size
    """
    def __init__(
        self,
        input_size,
        input_length,
        hidden_layer_num,
        batch_size,
        device,
        hidden_dense_dim = None,
        z_size = 3,
        hidden_rnn_size=500,
        dropout = 0,
        cell_type = "basic_rnn",
        planar_NF_length=0,
        epsilon=1e-4,
        planar_NF_size = 20
    ):
        """
        Init VAE model with GRU for encoder and decoder
        """
        super().__init__()
        self.hidden_rnn_size = hidden_rnn_size
        self.input_size = input_size
        self.input_length = input_length
        self.hidden_layer_num = hidden_layer_num
        self.cell_type = cell_type
        self.dropout = dropout
        self.z_size = z_size
        self.planar_NF_length = planar_NF_length
        self.epsilon = epsilon
        self.batch_size = batch_size
        self.device=device
        self.planar_NF_size = planar_NF_size
        if hidden_dense_dim == None:
            self.hidden_dense_dim = z_size
        else:
            self.hidden_dense_dim = hidden_dense_dim

        if self.cell_type == "basic_rnn":
            self.encode_cell = nn.RNN(
                input_size = self.input_size,
                hidden_size = self.hidden_rnn_size,
                num_layers = self.hidden_layer_num,
                dropout = self.dropout
            )
            self.decode_cell = nn.RNN(
                input_size = self.z_size,
                hidden_size = self.hidden_rnn_size,
                num_layers = self.hidden_layer_num,
                dropout = self.dropout
            )
        elif self.cell_type == "lstm":
            self.encode_cell = nn.LSTM(
                input_size = self.input_size,
                hidden_size = self.hidden_rnn_size,
                num_layers = self.hidden_layer_num,
                dropout = self.dropout
            )
            self.decode_cell = nn.LSTM(
                input_size = self.z_size,
                hidden_size = self.hidden_rnn_size,
                num_layers = self.hidden_layer_num,
                dropout = self.dropout
            )
        elif self.cell_type == "gru":
            self.encode_cell = nn.GRU(
                input_size = self.input_size,
                hidden_size = self.hidden_rnn_size,
                num_layers = self.hidden_layer_num,
                dropout = self.dropout
            )
            self.decode_cell = nn.GRU(
                input_size = self.z_size,
                hidden_size = self.hidden_rnn_size,
                num_layers = self.hidden_layer_num,
                dropout = self.dropout
            )        
        else:
            print("cell_type wrong!")

        self.encode_dense = nn.Linear(
            self.hidden_rnn_size + self.z_size,
            self.hidden_dense_dim
        )
        self.decode_dense = nn.Linear(
            self.hidden_rnn_size,
            self.hidden_dense_dim
        )

        self.encode_dense_muy = nn.Linear(
            self.hidden_dense_dim,
            self.z_size
        )
        self.decode_dense_muy = nn.Linear(
            self.hidden_dense_dim,
            self.input_size
        )
        self.encode_dense_sigma = nn.Linear(
            self.hidden_dense_dim,
            self.z_size
        )
        self.decode_dense_sigma = nn.Linear(
            self.hidden_dense_dim,
            self.input_size
        )
        self.encode_softplus = nn.Softplus(threshold=2)
        self.decode_softplus = nn.Softplus(threshold=2)
        if self.planar_NF_length == 0:
            self.planar_NF = nn.Identity()
        else:
            
            self.planar_NF_w = nn.ModuleList(
                nn.Linear(
                    self.z_size,
                    self.planar_NF_size
                )
                for i in range(self.planar_NF_length)
            )
            self.planar_NF_u = nn.ModuleList(
                nn.Linear(
                    self.planar_NF_size,
                    self.z_size
                )
                for i in range(self.planar_NF_length)
            )
            
            self.planar_NF_tanh = nn.ModuleList(
                nn.Tanh()
                for i in range(self.planar_NF_length)
            )
        self.decode_T = nn.Linear(
            z_size,
            z_size
        )
        self.decode_O = nn.Linear(
            z_size,
            z_size
        )
        self.e_initial = torch.zeros(
            (self.hidden_layer_num, self.batch_size, self.hidden_rnn_size)
        ).to(device=self.device)
        self.z_initial = torch.randn(
            (self.batch_size, self.z_size)
        ).to(device=self.device)

        self.d_initial = torch.zeros(
            (self.hidden_layer_num, self.batch_size, self.hidden_rnn_size)
        ).to(device=self.device)
    # def SSM(self, z_previous, z_observation, layer):
    def planar_NF_flow(self, z_pre, tf):
        next_z = z_pre + self.planar_NF_u[tf](
              self.planar_NF_tanh[tf](
                self.planar_NF_w[tf](z_pre)
              )
            )
        return next_z
    def encode(self, x):
        L = x.size()[0]
        if self.cell_type == "basic_rnn":
            self.e_t, _ = self.encode_cell(
                x,
                self.e_initial
            )
        elif self.cell_type == "gru":
            self.e_t, _ = self.encode_cell(
                x,
                self.e_initial
            )
        else: # lstm
            pass

        last_z = self.z_initial

        self.encode_z_e_concat = [None] * L
        self.encode_dense_out = [None] * L
        self.encode_muy_z = [None] * L
        self.encode_sigma_z = [None] * L
        self.encode_z = [None] * L
        self.encode_z_PNF = [[None] * (self.planar_NF_length + 1)] * L

        for l in range(L):
            self.encode_z_e_concat[l] = torch.cat([self.e_t[l,:,:], last_z], dim=1)
            self.encode_dense_out[l] = self.encode_dense(self.encode_z_e_concat[l])
            self.encode_muy_z[l] = self.encode_dense_muy(self.encode_dense_out[l])
            self.encode_sigma_z[l] = self.encode_softplus(
              self.encode_dense_sigma(self.encode_dense_out[l])
            ) + self.epsilon
            
            epsilon_encode = torch.randn_like(self.encode_muy_z[l])
            self.encode_z[l] = self.encode_muy_z[l] + epsilon_encode*self.encode_sigma_z[l]

            self.encode_z_PNF[l][0] = self.encode_z[l]
            if self.planar_NF_length !=0:
                for tf in range(self.planar_NF_length):
                    self.encode_z_PNF[l][tf+1] = self.planar_NF_flow(
                        self.encode_z_PNF[l][tf], 
                        tf
                    )
            last_z = self.encode_z_PNF[l][self.planar_NF_length]

        self.z = torch.stack([self.encode_z_PNF[i][self.planar_NF_length] for i in range(L)])
        self.muy_z = torch.stack(self.encode_muy_z)
        self.sigma_z = torch.stack(self.encode_sigma_z)
        return self.z, self.muy_z, self.sigma_z

    def SSM(self, z_input, z_observation):
        ob_err = torch.randn_like(z_observation)
        return self.decode_O(self.decode_T(z_input) + z_observation)# + ob_err

    def decode(self, z):
        L = z.shape[0]
        self.decode_z_input = [None] * L
        last_z = self.z_initial
        for l in range(L):
            self.decode_z_input[l] = self.SSM(self.decode_T(last_z), z[l,:,:])
            last_z = self.decode_z_input[l]
        self.z_input = torch.stack(self.decode_z_input)

        if self.cell_type == "basic_rnn":
            self.d_t, _ = self.decode_cell(
                self.z_input,
                self.d_initial
            )
        elif self.cell_type == "gru":
            self.d_t, _ = self.decode_cell(
                self.z_input,
                self.d_initial
            )
        else: # lstm
            pass

        self.decode_dense_out = [None] * L
        self.decode_muy_x_hat = [None] * L
        self.decode_sigma_x_hat = [None] * L
        self.decode_x_hat = [None] * L

        for l in range(L):
            self.decode_dense_out[l] = self.decode_dense(self.d_t[l,:,:])
            self.decode_muy_x_hat[l] = self.decode_dense_muy(self.decode_dense_out[l])
            self.decode_sigma_x_hat[l] = self.decode_softplus(self.decode_dense_sigma(self.decode_dense_out[l]) + self.epsilon) 
            self.epsilon_decode = torch.randn_like(self.decode_sigma_x_hat[l])
            self.decode_x_hat[l] = self.decode_muy_x_hat[l] + self.epsilon_decode*self.decode_sigma_x_hat[l]

            # print(self.decode_muy_x_hat[l])

        self.x_hat = torch.stack(self.decode_x_hat)
        self.muy_x_hat = torch.stack(self.decode_muy_x_hat)
        self.sigma_x_hat = torch.stack(self.decode_sigma_x_hat)
        # print(self.x_hat)
        
        return self.x_hat, self.muy_x_hat, self.sigma_x_hat

    def forward(self, x):
        z, muy_z, sigma_z = self.encode(x)
        x_hat, muy_x_hat, sigma_x_hat = self.decode(z)
        return x_hat, muy_z, sigma_z, z, muy_x_hat, sigma_x_hat

# input_size= 10
# input_length = 11
# hidden_layer_num = 12
# batch_size = 13
# device = torch.device('cpu')
# hidden_dense_dim = None
# z_size = 3
# hidden_rnn_size=500
# dropout = 0
# cell_type = "basic_rnn"
# planar_NF_length=0
# epsilon=1e-4
# planar_NF_size = 20


# vae =VAE(
#         input_size=input_size,
#         input_length=input_length,
#         hidden_layer_num=hidden_layer_num,
#         batch_size=batch_size,
#         device=device,
#         hidden_dense_dim = None,
#         z_size = 3,
#         hidden_rnn_size=500,
#         dropout = 0,
#         cell_type = "basic_rnn",
#         planar_NF_length=0,
#         epsilon=1e-4,
#         planar_NF_size = 20
#     )

# x = torch.randn((input_length, batch_size, input_size))

# x_hat, muy_z, sigma_z, z, muy_x_hat, sigma_x_hat = vae(x)

# print(x_hat.size())


# L = torch.sum((x_hat - 1)**2)
# x_hat.register_hook(lambda grad: print(grad))
# L.backward()

# L = torch.sum((muy_z - 1)**2)
# muy_z.register_hook(lambda grad: print(grad))
# L.backward()

# L = torch.sum((sigma_z - 1)**2)
# sigma_z.register_hook(lambda grad: print(grad))
# L.backward()

# L = torch.sum((z - 1)**2)
# z.register_hook(lambda grad: print(grad))
# L.backward()

# L = torch.sum((muy_x_hat - 1)**2)
# muy_x_hat.register_hook(lambda grad: print(grad))
# L.backward()

# L = torch.sum((sigma_x_hat - 1)**2)
# sigma_x_hat.register_hook(lambda grad: print(grad))
# L.backward()
