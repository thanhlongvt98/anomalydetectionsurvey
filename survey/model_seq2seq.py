from numpy.lib.function_base import select
import torch
import numpy as np
from torch._C import device 
import torch.nn as nn 

class Seq2Seq(nn.Module):
    def __init__(
        self,
        input_dim,
        rnn_layers_num,
        rnn_hidden_size,
        embedding_size,
        batch_size,
        input_length,
        device,
        cell_type="gru"
    ):
        super().__init__()
        self.input_dim = input_dim
        self.rnn_hidden_size = rnn_hidden_size
        self.rnn_layers_num = rnn_layers_num
        self.batch_szie = batch_size
        self.input_length = input_length
        self.cell_type = cell_type
        self.device = device
        self.embedding_size= embedding_size
        if cell_type=="gru":
            self.encode_cell = nn.GRU(
                input_size = self.embedding_size,
                hidden_size = self.rnn_hidden_size,
                num_layers = self.rnn_layers_num
            )
            self.decode_cell = nn.GRU(
                input_size = self.embedding_size,
                hidden_size = self.rnn_hidden_size,
                num_layers = self.rnn_layers_num
            )
        elif cell_type == "lstm":
            self.encode_cell = nn.LSTM(
                input_size = self.embedding_size,
                hidden_size = self.rnn_hidden_size,
                num_layers = self.rnn_layers_num
            )

            self.decode_cell = nn.LSTM(
                input_size = self.embedding_size,
                hidden_size = self.rnn_hidden_size,
                num_layers = self.rnn_layers_num
            )
        elif cell_type == "basic_rnn":
            self.encode_cell = nn.RNN(
                input_size = self.embedding_size,
                hidden_size = self.rnn_hidden_size,
                num_layers = self.rnn_layers_num
            )
            self.decode_cell = nn.RNN(
                input_size = self.embedding_size,
                hidden_size = self.rnn_hidden_size,
                num_layers = self.rnn_layers_num
            )
        self.encode_embedding = nn.Linear(
            self.input_dim,
            self.embedding_size
        )
        self.decode_embedding = nn.Linear(
            self.input_dim,
            self.embedding_size
        )
        self.decode_out = nn.Linear(
            self.rnn_hidden_size,
            self.input_dim
        )
        self.initial_encode_hidden = torch.zeros(
            (self.rnn_layers_num, self.batch_szie, self.rnn_hidden_size),
            device=self.device
        )
        
        self.initial_decode_x = torch.zeros(
            (1, self.batch_szie, self.input_dim),
            device=self.device
        )
        self.decode_reconstruction = torch.zeros(
            (self.input_length, self.batch_szie, self.input_dim), 
            device=self.device
        )
        self.decode_h = torch.zeros(
            (self.rnn_layers_num, self.batch_szie, self.rnn_hidden_size),
            device=self.device
        )
        for parameter in self.parameters():
            nn.init.normal_(parameter, 0.0, 0.01)
    def encode(self, x):
        embedding = self.encode_embedding(x)
        if self.cell_type=="gru":
            self.hidden_encode, self.encode_h = self.encode_cell(
                embedding,
                self.initial_encode_hidden
            )
        elif self.cell_type == "lstm":
            pass
        elif self.cell_type == "basic_rnn":
            self.hidden_encode, self.encode_h = self.encode_cell(
                embedding,
                self.initial_encode_hidden
            )
        return self.encode_h
    def decode(self, input, h):
        output_rnn = None
        input_embedded = self.decode_embedding(input)
        input_shifted = torch.cat(
            [
                torch.zeros((1, self.batch_szie, self.embedding_size), device=self.device), 
                input_embedded[1:,:,:]
            ], 
            dim=0
        )

        if self.cell_type=="gru" or self.cell_type=="basic_rnn":
            output_rnn, _ = self.decode_cell(
                input_shifted,
                h
            )

        elif self.cell_type == "lstm":
            pass
        reconstruction = self.decode_out(output_rnn)

        return reconstruction
    def forward(self, x):
        hidden_feature = self.encode(x)
        reconstruction = self.decode(x, hidden_feature)
        return reconstruction

torch.autograd.set_detect_anomaly(True)

# D = 38
# N = 64
# L = 128
# E_D = 256
# H = 512
# L_N = 3

# model = Seq2Seq(
#     input_dim=D,
#     rnn_layers_num=L_N,
#     rnn_hidden_size=H,
#     embedding_size=E_D,
#     batch_size=N,
#     input_length=L,
#     cell_type="basic_rnn",
#     device=torch.device('cpu')
# )

# input = torch.rand((L, N, D))
# out = model(input)
# print(input.shape)
# print(out.shape)

