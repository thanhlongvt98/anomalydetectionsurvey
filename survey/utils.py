import os 
import pickle 
import numpy as np 
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt

SMD_prefix = "./dataset/ServerMachineDataset/processed/"

def save_z(z, filename='z'):
    """
    Save the sampled z in a txt file

    Args:
        z: L x N x D
    Return:
        None: None
    """
    L, N, D = z.shape
    for n in range(0, N, 20):
        with open(filename + '_' + str(n) + '.txt', 'w') as  file:
            for l in range(L):
                for d in range(D): 
                    file.write('%f' % (z[l][n][d]))
                file.write('\n')

def get_data(name, train_end=None, train_start=0, test_start=0, test_end=None, load_dim=None):
    train_data = None 
    test_data = None
    test_label = None

    if name.startswith("machine"):
        x_dim = 38
        f = open(os.path.join(SMD_prefix, name + '_train.pkl'), "rb")
        train_data = pickle.load(f).reshape((-1, x_dim))[train_start:train_end, 0:load_dim]
        f.close()
        f = open(os.path.join(SMD_prefix, name + '_test.pkl'), "rb")
        test_data = pickle.load(f).reshape((-1, x_dim))[test_start:test_end, 0:load_dim]
        f.close()
        f = open(os.path.join(SMD_prefix, name + '_test_label.pkl'), "rb")
        test_label = pickle.load(f).reshape((-1))[test_start:test_end]

    return (train_data, None), (test_data, test_label)

def plot_data(dataset, num_samples, num_dim, train=True):
    """
    Plot data

    Args:
    dataset: (). dataset to show
    num_samples: int. Number of sample to show
    num_dim: int. Number of dim to show
    """
    (train_data, _), (test_data, _) = get_data(
        dataset, train_end=num_samples, load_dim=num_dim)
    
    fig, (ax) = plt.subplots(num_dim, sharex=True, figsize=(32,2*num_dim))
    fig.suptitle('Training data ')
    
    
    for dim in range(num_dim):
        if train:
            ax[dim].plot(np.arange(num_samples), train_data[0:num_samples, dim])
        else:
            ax[dim].plot(np.arange(num_samples), test_data[0:num_samples, dim])

    plt.show()